const authors = [{ name: "J.K. Rowling" }, { name: "Christopher Moore" }];
const books = [
  {
    title: "Harry Potter and the Philosopher's Stone",
    genre: "Fantasy",
    authorId: 1,
  },
  {
    title: "Fantastic Beasts and Where to Find",
    genre: "Fantasy",
    authorId: 1,
  },
  {
    title: "A Dirty Job: A Novel",
    genre: "Fantasy",
    authorId: 2,
  },
  {
    title: "Shakespeare for Squirrels: A Novel",
    genre: "Fantasy",
    authorId: 2,
  },
];

exports.authors = authors;
exports.books = books;
