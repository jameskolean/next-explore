// prisma/seed.js
const { PrismaClient } = require("@prisma/client");
const { authors, books } = require("./data.js");
const prisma = new PrismaClient();

const load = async () => {
  try {
    console.log("Seeding data...");
    await prisma.book.deleteMany();
    await prisma.author.deleteMany();
    console.log("Seeding data... deletes");
    await prisma.$queryRaw`ALTER TABLE Author AUTO_INCREMENT = 1`;
    await prisma.$queryRaw`ALTER TABLE Book AUTO_INCREMENT = 1`;

    console.log("Seeding data... reset");
    console.log("Seeding data... authors", authors);
    await prisma.author.createMany({
      data: authors,
    });
    console.log("Seeding data... 1");
    await prisma.book.createMany({
      data: books,
    });
    console.log("Seeding data... 2");
  } catch (e) {
    console.error(e);
    process.exit(1);
  } finally {
    await prisma.$disconnect();
  }
};
load();
