// src/app/[lng]/books/page.tsx
import Link from "next/link";
import { PrismaClient } from "@prisma/client";
const prisma = new PrismaClient();

interface Props {
  params: {
    lng: string;
  };
}
export default async function SecondPage(props: Props) {
  const books = await prisma.book.findMany();
  return (
    <main style={{ display: "flex", flexDirection: "column", gap: "1.5em" }}>
      <h1>Books</h1>
      <Link href={`/${props.params.lng}/`}>Home</Link>
      <section>
        {books.map((book) => (
          <div key={book.id}>
            <h2>{book.title}</h2>
          </div>
        ))}
      </section>
    </main>
  );
}
