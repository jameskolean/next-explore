import styles from "./page.module.css";
import Link from "next/link";
import { useTranslation } from "@/app/i18n";

interface Props {
  params: {
    lng: string;
  };
}
export default async function Home(props: Props) {
  const { t } = await useTranslation(props.params.lng, "translation");
  return (
    <main>
      <h1>{t("title")}</h1>
      <Link href={`/${props.params.lng}/second-page`}>
        {t("to-second-page")}
      </Link>
      <br />
      <Link href={`/${props.params.lng}/books`}>To books page</Link>
      <br />
      <Link href={`/api/graphql`}>Open Graphiql</Link>
    </main>
  );
}
