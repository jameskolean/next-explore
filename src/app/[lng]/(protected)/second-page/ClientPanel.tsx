"use client";
import { useTranslation } from "@/app/i18n/client";
import { useEffect, useState } from "react";

interface Props {
  lng: string;
}
export default function ClientPanel(props: Props) {
  const [data, setData] = useState(null);
  const [isLoading, setLoading] = useState(true);

  useEffect(() => {
    fetch("https://next-explore-lu3zhesl6-jameskolean.vercel.app/api/greet")
      .then((res) => res.json())
      .then((data) => {
        setData(data);
        setLoading(false);
      });
  }, []);
  const { t } = useTranslation(props.lng, "client-component");
  return (
    <div>
      {t("title")}
      <pre>{JSON.stringify(data, null, 2)}</pre>
    </div>
  );
}
