"use client";
// src/app/[lng]/(protected)/second-page/ClientSessionPanel.tsx
import useSession from "@/session/use-session";
import { useEffect } from "react";
import { useRouter } from "next/navigation";

interface Props {
  lng: string;
}
export default function ClientSessionPanel(props: Props) {
  const { session, isLoading } = useSession();
  const router = useRouter();
  // I don't think we need this, the middleware should be enough.
  useEffect(() => {
    if (!isLoading && !session.isLoggedIn) {
      router.replace(`/${props.lng}/login`);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [isLoading, session.isLoggedIn, router]);

  if (isLoading) {
    return <p>Loading...</p>;
  }

  return (
    <div>
      <p>
        Hello <strong>{session.username}!</strong>
      </p>
    </div>
  );
}
