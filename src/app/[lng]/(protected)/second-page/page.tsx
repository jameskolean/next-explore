// src/app/[lng]/second-page/page.tsx
import Link from "next/link";
import { useTranslation } from "@/app/i18n";
import ClientPanel from "./ClientPanel";
import ClientSessionPanel from "./ClientSessionPanel";

interface Props {
  params: {
    lng: string;
  };
}
export default async function SecondPage(props: Props) {
  const { t } = await useTranslation(props.params.lng, "second-page");
  return (
    <main style={{ display: "flex", flexDirection: "column", gap: "1.5em" }}>
      <h1>{t("title")}</h1>
      <Link href={`/${props.params.lng}/`}> {t("to-home")}</Link>
      <div>
        <h2>See a client rendered component</h2>
        <ClientPanel lng={props.params.lng} />
      </div>
      <div>
        <h2>See session data rendered from client component</h2>
        <ClientSessionPanel lng={props.params.lng} />
      </div>
      <div>
        <h2>Call an API</h2>
        <form
          action="/api/greet"
          style={{ display: "flex", flexDirection: "column", gap: "0.5em" }}
          method="get"
        >
          <label htmlFor="username">Name:</label>
          <input type="text" id="username" name="username" />
          <button type="submit" style={{ width: "fit-content" }}>
            Submit
          </button>
        </form>
      </div>
    </main>
  );
}
