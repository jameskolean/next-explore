// src/app/[lng]/layout.tsx
import type { Metadata } from "next";
import { Inter } from "next/font/google";
import "./globals.css";
import { dir } from "i18next";
import { PropsWithChildren } from "react";
import { languages } from "@/app/i18n/settings";
import { getIronSession } from "iron-session";
import { SessionData, sessionOptions } from "@/session";
import { cookies } from "next/headers";

export async function generateStaticParams() {
  return languages.map((lng) => ({ lng }));
}
const inter = Inter({ subsets: ["latin"] });

export const metadata: Metadata = {
  title: "Next Explorer",
  description: "App to explore Next 13",
};
interface Props {
  children: React.ReactNode;

  params: {
    lng: string;
  };
}
export default async function RootLayout(props: PropsWithChildren<Props>) {
  const session = await getIronSession<SessionData>(cookies(), sessionOptions);
  return (
    <html lang={props.params.lng} dir={dir(props.params.lng)}>
      <head />
      <body className={inter.className}>
        <div>
          {!!session && !!session.username && (
            <div style={{ display: "flex", gap: "2rem", alignItems: "center" }}>
              Hello {session.username}
              <LogoutButton />
            </div>
          )}
        </div>
        <div>{props.children}</div>
      </body>
    </html>
  );
}
function LogoutButton() {
  return (
    <p>
      <a href="/api/session?action=logout">Logout</a>
    </p>
  );
}
