"use client";
// src/app/[lng]/login/form.tsx
import { useEffect, useState } from "react";
import { SessionData } from "@/session";
import { defaultSession } from "@/session";

export function Form() {
  const [session, setSession] = useState<SessionData>(defaultSession);
  const [isLoading, setIsLoading] = useState(true);

  useEffect(() => {
    fetch("/api/session")
      .then((res) => res.json())
      .then((session) => {
        setSession(session);
        setIsLoading(false);
      });
  }, []);

  if (isLoading) {
    return <p>Loading...</p>;
  }

  if (session.isLoggedIn) {
    return (
      <>
        <p>
          Logged in user: <strong>{session.username}</strong>
        </p>
        <LogoutButton />
      </>
    );
  }

  return <LoginForm />;
}

function LoginForm() {
  return (
    <form action="/api/session" method="POST">
      <label className="block text-lg">
        <span>Username</span>
        <input
          type="text"
          name="username"
          placeholder=""
          defaultValue="James"
          required
          // for demo purposes, disabling autocomplete 1password here
          // autoComplete="off"
          // data-1p-ignore
        />
      </label>
      <div>
        <input type="submit" value="Login" />
      </div>
    </form>
  );
}

function LogoutButton() {
  return (
    <p>
      <a href="/api/session?action=logout">Logout</a>
    </p>
  );
}
