// src/app/[lng]/login/page.tsx
import { Metadata } from "next";
import { Form } from "./form";

export const metadata: Metadata = {
  title: "Login",
};

export default function AppRouterRedirect() {
  return (
    <main>
      <h1>Login</h1>
      <div>
        <Form />
      </div>
    </main>
  );
}
