// src/app/api/greet/route.ts

import { cookies } from "next/headers";
import { getIronSession } from "iron-session";

export async function GET(request: Request) {
  const { searchParams } = new URL(request.url);
  const username = searchParams.get("username");
  return Response.json({ greeting: `hello ${username}` });
}
export async function POST(request: Request) {
  const { searchParams } = new URL(request.url);
  const username = searchParams.get("username");
  return Response.json({ greeting: `hello ${username}` });
}
// Not needed since we use the search params
// export const dynamic = "force-dynamic"; // defaults to force-static
