// src/graphql/resolvers.ts
import prisma from "@/lib/prisma";

export const resolvers = {
  Query: {
    books: async () => {
      return await prisma.book.findMany({
        include: {
          author: true, // All posts where authorId == 20
        },
      });
    },
    authors: async () => {
      return await prisma.author.findMany({
        include: {
          books: true, // All posts where authorId == 20
        },
      });
    },
  },
};
