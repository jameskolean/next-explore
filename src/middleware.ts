import { middlewareChain } from "@/middlewares/MiddlewareChain";
import { withLogger } from "@/middlewares/withLogger";
import { withInternalization } from "@/middlewares/withInternationalization";
import { withSession } from "@/middlewares/withSession";
import { withCors } from "@/middlewares/withCors";

const middlewares = [withLogger, withInternalization, withSession, withCors];
export default middlewareChain(middlewares);

export const config = {
  matcher: [
    /*
     * Match all request paths except for the ones starting with:
     * - _next/static (static files)
     * - _next/image (image optimization files)
     * - _next/assets (asset files)
     * - favicon.ico (favicon file)
     */
    "/((?!_next/static|_next/image|assets|favicon.ico|sw.js).*)",
  ],
};
