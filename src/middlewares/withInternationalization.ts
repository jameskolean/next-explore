import acceptLanguage from "accept-language";
import { fallbackLng, languages, cookieName } from "@/app/i18n/settings";

import { NextFetchEvent, NextRequest, NextResponse } from "next/server";
import { MiddlewareFactory } from "./middlewareFactory";

acceptLanguage.languages(languages);
export const withInternalization: MiddlewareFactory = (next) => {
  return async (request: NextRequest, _next: NextFetchEvent) => {
    let lng;
    if (request.cookies.has(cookieName))
      lng = acceptLanguage.get(request.cookies.get(cookieName)?.value);
    if (!lng) lng = acceptLanguage.get(request.headers.get("Accept-Language"));
    if (!lng) lng = fallbackLng;

    // Redirect if lng in path is not supported
    if (
      !languages.some((loc: string) =>
        request.nextUrl.pathname.startsWith(`/${loc}`)
      ) &&
      !request.nextUrl.pathname.startsWith("/_next") &&
      !request.nextUrl.pathname.startsWith("/api")
    ) {
      return NextResponse.redirect(
        new URL(`/${lng}${request.nextUrl.pathname}`, request.url)
      );
    }

    if (request.headers.has("referer")) {
      const refererUrl = new URL(request.headers.get("referer")!);
      const lngInReferer = languages.find((l) =>
        refererUrl.pathname.startsWith(`/${l}`)
      );
      const response = NextResponse.next();
      if (lngInReferer) response.cookies.set(cookieName, lngInReferer);
      return response;
    }
    return next(request, _next);
  };
};
