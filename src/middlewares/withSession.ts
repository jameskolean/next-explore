// src/middlewares/withSession.ts
import { NextFetchEvent, NextRequest } from "next/server";
import { MiddlewareFactory } from "./middlewareFactory";
import { cookies } from "next/headers";
import { getIronSession } from "iron-session";
import { cookieName as i18nCookieName } from "@/app/i18n/settings";
import { SessionData, sessionOptions } from "@/session";

export const withSession: MiddlewareFactory = (next) => {
  return async (request: NextRequest, _next: NextFetchEvent) => {
    const session = await getIronSession<SessionData>(
      cookies(),
      sessionOptions
    );
    const lng = request.cookies.get(i18nCookieName)?.value;
    if (
      !session.isLoggedIn &&
      !request.nextUrl.pathname.endsWith("login") &&
      !request.nextUrl.pathname.startsWith("/api/")
    ) {
      const redirectTo = `/${lng}/login`;
      return Response.redirect(`${request.nextUrl.origin}${redirectTo}`, 302);
    }
    return next(request, _next);
  };
};
