// src/session/index.ts
import { SessionOptions } from "iron-session";

export interface SessionData {
  username: string;
  isLoggedIn: boolean;
}
export const sessionOptions: SessionOptions = {
  // should come from env
  password: "complex_password_at_least_32_characters_long",
  cookieName: "iron-session",
  cookieOptions: {
    // secure only works in `https` environments
    // if your localhost is not on `https`, then use: `secure: process.env.NODE_ENV === "production"`
    secure: process.env.NODE_ENV === "production",
    // secure: true,
  },
};
export const defaultSession: SessionData = {
  username: "",
  isLoggedIn: false,
};

export function sleep(ms: number) {
  return new Promise((resolve) => setTimeout(resolve, ms));
}
